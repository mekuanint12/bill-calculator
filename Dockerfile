FROM golang:1.21.6

WORKDIR /app

RUN apt-get update && apt-get install -y 

COPY go.mod go.sum ./

RUN go mod download

COPY *.go ./

RUN go build -o app

EXPOSE 8080

CMD ["./app"]
`