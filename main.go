package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func getInput(prompt string, r *bufio.Reader) (string, error) {
	fmt.Println(prompt)
	input, err := r.ReadString('\n')

	return strings.TrimSpace(input), err
}

func createBill() bill {
	reader := bufio.NewReader(os.Stdin)
	// fmt.Println("Create a new bill..")
	// name, _ := reader.ReadString('\n')
	// name = strings.TrimSpace(name)
	name, _ := getInput("Create a new bill name:", reader)
	b := newBill(name)
	fmt.Println("Created the bill for :", b.name)
	return b
}

func promptOptions(b bill) {
	reader := bufio.NewReader(os.Stdin)
	opt, _ := getInput("Choose option(a - add Item, s - save the Bill, t - add Tip)", reader)
	switch opt {
	case "a":
		name, _ := getInput("Item name: ", reader)
		price, _ := getInput("Price: ", reader)
		p, err := strconv.ParseFloat(price, 64)
		if err != nil {
			fmt.Println("The price is not a number, please correct your input")
			promptOptions(b)
		}
		b.addItem(name, p)
		fmt.Println("Item Added successfully -", name, "- ", price)
		promptOptions(b)
	case "t":
		tip, _ := getInput("Add Tip : ", reader)
		t, err := strconv.ParseFloat(tip, 64)
		if err != nil {
			fmt.Println("The tip is not a number, please correct your input")
			promptOptions(b)
		}
		b.updateTip(t)
		fmt.Println("Tip Added successfully -", "$", t)
		promptOptions(b)
	case "s":
		b.save()
		fmt.Println("You Saved the Bill - ", b.name)
	default:
		fmt.Println("Sorry, You Choose Invalid Letter")
		promptOptions(b)
	}
}

func main() {
	mybill := createBill()
	promptOptions(mybill)
	fmt.Println(mybill)
}
